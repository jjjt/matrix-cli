#!/usr/bin/perl

use strict;
use warnings;
use LWP::UserAgent;

print "Username:";
my $userID = <STDIN>;
chomp $userID;
print $userID;

my $ua = LWP::UserAgent->new(ssl_opts => { verify_hostname => 1 });
my $res = $ua->get("https://matrix.org/_matrix/client/versions");

# specify a CA path
$ua = LWP::UserAgent->new(
	ssl_opts => {
		SSL_ca_path	=> '/etc/ssl/certs',
		verify_hostname	=> 1,
	}
);

use Data::Dumper; print Dumper $res;
