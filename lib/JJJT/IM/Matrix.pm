package JJJT::IM::Matrix;

use strict;
use warnings;

# Class: Matrix
# Author: JJJT
#
# provides API for communication with the Matrix network via class

# Constructor: new
# args:
#	username
#	homeserver
#	password

sub new {
	my ( $class, $user, $home, $pass ) = @_;
	my $self = bless {
		password   => $pass,
		userid	   => $user,
		homeserver => $home
	}, $class;
}

1;
